// https://docs.vuestorefront.io/guide/vuex/vuex-conventions.html

// Use snake_case for writing state property
export const state = () => ({
  id: 0,
  name: '',
  lecturerName: '',
  information: '',
  quizzes: []
});

// Getters Start from "is" when returns Boolean, or "get" otherwise
export const getters = {
  getCourseId: (state) => state.id,
  getCourseName: (state) => state.name,
  getCourseLecturerName: (state) => state.lecturerName,
  getCourseInformation: (state) => state.information,
  getQuizzes: (state) => state.quizzes
};

// Their names should be as unique as possible and simply describe what specific action will happen.
export const actions = {
  async fetchClassQuizzes({commit, getters}) {

    const {data} = await this.$axios.put(`/courses/${getters.getCourseId}`, classData);
    console.log(data);
    commit('SET_ID', data.id);
    commit('SET_NAME', data.name);
    commit('SET_LECTURER_NAME', data.lecturerName);
    commit('SET_INFORMATION', data.information);
  },
  async updateClassInformation({commit, getters}, classData) {

    const {data} = await this.$axios.put(`/courses/${getters.getCourseId}`, classData);
    console.log(data);
    commit('SET_ID', data.id);
    commit('SET_NAME', data.name);
    commit('SET_LECTURER_NAME', data.lecturerName);
    commit('SET_INFORMATION', data.information);
  },
};

// Use SCREAMING_SNAKE_CASE and should start with one of SET_ or ADD_ or REMOVE_
export const mutations = {
  SET_ID(state, value) {
    state.id = value;
  },
  SET_NAME(state, value) {
    state.name = value;
  },
  SET_LECTURER_NAME(state, value) {
    state.lecturerName = value;
  },
  SET_INFORMATION(state, value) {
    state.information = value;
  },
};
