// https://docs.vuestorefront.io/guide/vuex/vuex-conventions.html

// Use snake_case for writing state property
export const state = () => ({
  user_authenticated: false,
  drawer_opened: false
});

// Getters Start from "is" when returns Boolean, or "get" otherwise
export const getters = {
  isUserAuthenticated: (state) => state.user_authenticated,
  isDrawerOpened: (state) => state.drawer_opened
};

// Their names should be as unique as possible and simply describe what specific action will happen.
export const actions = {
  setUserAuthenticated({commit}, value) {
    commit('SET_AUTHENTICATED', value);
  },
  toggleDrawer({commit, getters}) {
    commit('SET_DRAWER_OPENED', !getters.isDrawerOpened);
  },
  setDrawer({commit}, value) {
    commit('SET_DRAWER_OPENED', value);
  }
};

// Use SCREAMING_SNAKE_CASE and should start with one of SET_ or ADD_ or REMOVE_
export const mutations = {
  SET_AUTHENTICATED(state, value) {
    state.user_authenticated = value;
  },
  SET_DRAWER_OPENED(state, value) {
    state.drawer_opened = value;
  }
};





