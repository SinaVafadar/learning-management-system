// https://docs.vuestorefront.io/guide/vuex/vuex-conventions.html

// Use snake_case for writing state property
export const state = () => ({
  id: 0,
  title: '',
  information: '',
  questions: []
});

// Getters Start from "is" when returns Boolean, or "get" otherwise
export const getters = {
  getQuizId: (state) => state.id,
  getQuizTitle: (state) => state.title,
  getQuizInformation: (state) => state.information,
  getQuestions: (state) => state.information
};

// Their names should be as unique as possible and simply describe what specific action will happen.
export const actions = {
  async updateQuizInformation({commit, getters}, quizData) {

    const {data} = await this.$axios.put(`/quizzes/${getters.getQuizId}`, quizData);

    commit('SET_ID', data.id);
    commit('SET_TITLE', data.title);
    commit('SET_INFORMATION', data.information);
  },
};

// Use SCREAMING_SNAKE_CASE and should start with one of SET_ or ADD_ or REMOVE_
export const mutations = {
  SET_ID(state, value) {
    state.id = value;
  },
  SET_TITLE(state, value) {
    state.title = value;
  },
  SET_INFORMATION(state, value) {
    state.information = value;
  },
};
