// https://docs.vuestorefront.io/guide/vuex/vuex-conventions.html

// Use snake_case for writing state property
export const state = () => ({
  id: 0,
  full_name: '',
  profile: '',
  email: '',
  field: '',
  about: '',
  access: ''
});

// Getters Start from "is" when returns Boolean, or "get" otherwise
export const getters = {
  getUserId: (state) => state.id,
  getUserFullName: (state) => state.full_name,
  getUserProfile: (state) => state.profile,
  getUserEmail: (state) => state.email,
  getUserField: (state) => state.field,
  getUserAbout: (state) => state.about,
  getUserAccess: (state) => state.access,
};

// Their names should be as unique as possible and simply describe what specific action will happen.
export const actions = {
  async loginUser({commit}, loginData) {

    const response = await this.$axios.post('/users/login', loginData);
    console.log(response);

    this.$axios.interceptors.request.use(
      config => {
        config.headers.authorization = response.data;
        return config;
      },
      error => {
        return Promise.reject(error);
      }
    );

    const {data} = await this.$axios.get('/users/me');

    commit('SET_ID', data.id);
    commit('SET_FULL_NAME', data.fullName);
    commit('SET_PROFILE', data.profile); //TODO
    commit('SET_EMAIL', data.email);
    commit('SET_FIELD', data.field);
    commit('SET_ABOUT', data.about);
    commit('SET_ACCESS', data.access);

    await this.$router.push(`/users/${data.id}`);
    commit('SET_AUTHENTICATED', true, {root: true});
  },
  async updateUserInformation({commit, getters}, userData) {

    const {data} = await this.$axios.put(`/users/${getters.getUserId}`, userData);

    commit('SET_ID', data.id);
    commit('SET_FULL_NAME', data.fullName);
    commit('SET_EMAIL', data.email);
    commit('SET_FIELD', data.field);
    commit('SET_ABOUT', data.about);
  },
  async updateUserProfile({commit, getters}, profile) {
    try {
      const {data} = await this.$axios.put(`/users/profile/${getters.getUserId}`, profile); // todo
      // commit('SET_PROFILE', ``);
      // commit('SET_PROFILE', `http://localhost:8080/api/v1/users/${getters.getUserId}/profile`);
    } catch (error) {
      console.log(error.response)
    }
  },
};

// Use SCREAMING_SNAKE_CASE and should start with one of SET_ or ADD_ or REMOVE_
export const mutations = {
  SET_ID(state, value) {
    state.id = value;
  },
  SET_FULL_NAME(state, value) {
    state.full_name = value;
  },
  SET_PROFILE(state, value) {
    state.profile = value;
  },
  SET_EMAIL(state, value) {
    state.email = value;
  },
  SET_FIELD(state, value) {
    state.field = value;
  },
  SET_ABOUT(state, value) {
    state.about = value;
  },
  SET_ACCESS(state, value) {
    state.access = value;
  }
};





